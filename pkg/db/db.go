package db

import (
	"gitlab.com/azonnix/authentication/order-svc/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Handler struct {
	DB *gorm.DB
}

func Init(url string) (Handler, error) {
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{})
	if err != nil {
		return Handler{}, err
	}

	db.AutoMigrate(&models.Order{})

	return Handler{db}, nil
}
