package models

type Order struct {
	Id        int64 `json:"id" gorm:"primaryKey"`
	Price     int64 `json:"price"`
	ProductId int64 `json:"productId"`
	UserId    int64 `json:"userId"`
}
