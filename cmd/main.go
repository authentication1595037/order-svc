package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/azonnix/authentication/order-svc/pkg/client"
	confing "gitlab.com/azonnix/authentication/order-svc/pkg/config"
	"gitlab.com/azonnix/authentication/order-svc/pkg/db"
	"gitlab.com/azonnix/authentication/order-svc/pkg/pb"
	"gitlab.com/azonnix/authentication/order-svc/pkg/service"
	"google.golang.org/grpc"
)

func main() {
	c, err := confing.LoadConfig()
	if err != nil {
		log.Fatal(err.Error())
	}

	h, err := db.Init(c.DBUrl)
	if err != nil {
		log.Fatal(err.Error())
	}

	lis, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to listing:", err)
	}

	productSvc := client.InitProductServiceClient(c.ProductSvcUrl)

	fmt.Println("Order Svc on", c.Port)

	s := service.Server{
		H:          h,
		ProductSvc: productSvc,
	}

	grpcServer := grpc.NewServer()

	pb.RegisterOrderServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalln("Failed to serve:", err)
	}
}
